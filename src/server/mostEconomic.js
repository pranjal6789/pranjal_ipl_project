function mostEconomic(matches,deliveries){
    const arrayForMatchId=[];
    const runsOfBowler={};
    const totalRunsByPerBowler={};
    const totalOverByPerBowler={};
    const perBowlerEconomy={};
    const finalResultOfMostEconomy={};
    for(let index=0;index<matches.length;index++){
        if(matches[index].season==='2015'){
            arrayForMatchId.push(matches[index].id);
        }
    }
    //here we are finding the runs by each bowler//
    for(let index=0;index<arrayForMatchId.length;index++){
        for(let deliveryIndex=0;deliveryIndex<deliveries.length;deliveryIndex++){
            if(parseInt(arrayForMatchId[index])===parseInt(deliveries[deliveryIndex].match_id)){
                if(runsOfBowler[deliveries[deliveryIndex].bowler]){                
                    runsOfBowler[deliveries[deliveryIndex].bowler]+=parseInt(deliveries[deliveryIndex].total_runs)-(parseInt(deliveries[deliveryIndex].bye_runs)+parseInt(deliveries[deliveryIndex].legbye_runs)+parseInt(deliveries[deliveryIndex].penalty_runs));
                }else{
                    runsOfBowler[deliveries[deliveryIndex].bowler]=parseInt(deliveries[deliveryIndex].total_runs)-(parseInt(deliveries[deliveryIndex].bye_runs)+parseInt(deliveries[deliveryIndex].legbye_runs)+parseInt(deliveries[deliveryIndex].penalty_runs));
              }
           }
       }
    }
    //here we are finding the total runs by each bowler//
    for(let index=0;index<arrayForMatchId.length;index++){
        for(let deliveryIndex=0;deliveryIndex<deliveries.length;deliveryIndex++){
            if(parseInt(arrayForMatchId[index])===parseInt(deliveries[deliveryIndex].match_id)){
                if(deliveries[deliveryIndex].wide_runs==='0' && deliveries[deliveryIndex].noball_runs==='0'){
                    if(totalRunsByPerBowler[deliveries[deliveryIndex].bowler]){
                        totalRunsByPerBowler[deliveries[deliveryIndex].bowler]+=1;
                    }else{
                        totalRunsByPerBowler[deliveries[deliveryIndex].bowler]=1;
                    }
                }
            }
        }
    }
    //here we are finding the total over bowl by each bowler//
    for(let index=0;index<arrayForMatchId.length;index++){
        for(let deliveryIndex=0;deliveryIndex<deliveries.length;deliveryIndex++){
            if(parseInt(arrayForMatchId[index])===parseInt(deliveries[deliveryIndex].match_id)){
                totalOverByPerBowler[deliveries[deliveryIndex].bowler]=(totalRunsByPerBowler[deliveries[deliveryIndex].bowler]/6)+(totalRunsByPerBowler[deliveries[deliveryIndex].bowler]%6)/10;
            }
        }
    }
    // here we are calculating the economy//
    for(let index=0;index<arrayForMatchId.length;index++){
        for(let deliveryIndex=0;deliveryIndex<deliveries.length;deliveryIndex++){
            if(parseInt(arrayForMatchId[index])===parseInt(deliveries[deliveryIndex].match_id)){
                perBowlerEconomy[deliveries[deliveryIndex].bowler]=runsOfBowler[deliveries[deliveryIndex].bowler]/totalOverByPerBowler[deliveries[deliveryIndex].bowler];
            }
        }
    }
    let sortBowlerAccordingToEconomy = [];
    for (let player in perBowlerEconomy ) {
        sortBowlerAccordingToEconomy.push([player, perBowlerEconomy[player]]);
    }
    sortBowlerAccordingToEconomy.sort(function(firstValue, secondValue) {
        return firstValue[1] - secondValue[1];
    });
    for(let index=0;index<10;index++){
        finalResultOfMostEconomy[sortBowlerAccordingToEconomy[index][0]]=sortBowlerAccordingToEconomy[index][1];
    }
     return finalResultOfMostEconomy;// final result of 10 bowler who have least economy//
    }
module.exports=mostEconomic;