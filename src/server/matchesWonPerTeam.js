function matchesWonPerTeam(matches){
    let matchesWonByEachTeam={};
    let finalResultOfMatchesWonByEachTeam={};
    for(let item of matches){
            if (matchesWonByEachTeam[item.season])
            {
                if(matchesWonByEachTeam[item.season][item.winner])
               {
                   matchesWonByEachTeam[item.season][item.winner]+=1;
                }
                else
                {
                   matchesWonByEachTeam[item.season][item.winner]=1;
                }
            }
            else
            {
               matchesWonByEachTeam[item.season]={};
               matchesWonByEachTeam[item.season][item.winner]=1;
            }
         } 
    for(let index in matchesWonByEachTeam){
        for(let item in matchesWonByEachTeam[index]){
            //console.log(matchesWonByEachTeam[index][item]);
            if(matchesWonByEachTeam[index][item]!==""){
                finalResultOfMatchesWonByEachTeam[index]=matchesWonByEachTeam[index];
            }
        }
    }
    //console.log(finalResultOfMatchesWonByEachTeam);
    return finalResultOfMatchesWonByEachTeam;
}

module.exports=matchesWonPerTeam;