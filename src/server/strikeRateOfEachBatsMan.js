function strikeRateOfEachBatsMan(matchesData,deliveriesData){
    let batsmanRuns={};
    let playedBalls={};
    let strikeRate={};
    let finalResultOfStrikeRateOfEachBatsman={};
    for(let item of matchesData){
        for(let delivery of deliveriesData){
            if(item.id===delivery.match_id){
                if(batsmanRuns[item.season]){
                    if(batsmanRuns[item.season][delivery.batsman]){
                        batsmanRuns[item.season][delivery.batsman]+=parseInt(delivery.batsman_runs);
                    }else{
                        batsmanRuns[item.season][delivery.batsman]=parseInt(delivery.batsman_runs);
                    }
                }else{
                    batsmanRuns[item.season]={};
                    batsmanRuns[item.season][delivery.batsman_runs]=parseInt(delivery.batsman_runs);
                }
            }
        }

    }
    for(let item of matchesData){
        for(let delivery of deliveriesData){
            if(item.id===delivery.match_id){
                if(playedBalls[item.season]){
                    if(playedBalls[item.season][delivery.batsman]){
                        if(deliveriesData[item.wide_runs]!=='0' && deliveriesData[item.noball_runs]!=='0'){
                            playedBalls[item.season][delivery.batsman]+=1;
                        }    
                    }else{
                        if(deliveriesData[item.wide_runs]!=='0' && deliveriesData[item.noball_runs]!=='0'){
                            playedBalls[item.season][delivery.batsman]=1;
                        }    
                    }
                }else{
                    playedBalls[item.season]={};
                    //batsmanRuns[item.season][delivery.batsman_runs]=parseInt(delivery.batsman_runs);
                    playedBalls[item.season][delivery.batsman]=1;
                }
            }
        }

    }
    for(let item of matchesData){
        for(let delivery of deliveriesData){
            if(item.id===delivery.match_id){
                if(strikeRate[item.season]){
                    if(strikeRate[item.season][delivery.batsman]){
                        strikeRate[item.season][delivery.batsman]=batsmanRuns[item.season][delivery.batsman]/playedBalls[item.season][delivery.batsman]*100;
                   }else{
                       strikeRate[item.season][delivery.batsman]={};
                   }
                }
                else{
                    strikeRate[item.season]={};
                }     
            }
        }
    }    
    for(let index in strikeRate){
        let particularBatsman=strikeRate[index];
        for(let item in particularBatsman){
            if(item==="V Kohli"){
                finalResultOfStrikeRateOfEachBatsman[index]={"V Kohli":particularBatsman[item]};
            }
        }
    }
    return finalResultOfStrikeRateOfEachBatsman;
}
module.exports=strikeRateOfEachBatsMan;