function matchesPlayedPerYear(matchesData){
    const matchesPlayedPerYearBYTeams={};
    for(let item of matchesData){
        if(matchesPlayedPerYearBYTeams[item.season]){
            matchesPlayedPerYearBYTeams[item.season]+=1
        }else{
            matchesPlayedPerYearBYTeams[item.season]=1
        }
    }
    return matchesPlayedPerYearBYTeams;
}

module.exports=matchesPlayedPerYear;