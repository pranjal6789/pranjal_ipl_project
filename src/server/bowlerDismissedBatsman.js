function bowlerDismissedBatsman(deliveriesData){
    let dismissedBatsman={};
    for(let delivery of deliveriesData){
        if(dismissedBatsman[delivery.player_dismissed]){
            if(dismissedBatsman[delivery.player_dismissed][delivery.bowler]){
                if(delivery.player_dismissed!==""){
                    if(delivery.dismissal_kind!=="run out"){
                        dismissedBatsman[delivery.player_dismissed][delivery.bowler]+=1
                }
            }       
            }else{
                if(delivery.player_dismissed!==""){
                    if(delivery.dismissal_kind!=="run out"){
                        dismissedBatsman[delivery.player_dismissed][delivery.bowler]=1;
                }
            }     
            }
        }else{
            if(delivery.player_dismissed!==""){
                if(delivery.dismissal_kind!=="run out"){
                    dismissedBatsman[delivery.player_dismissed]={};
                    dismissedBatsman[delivery.player_dismissed][delivery.bowler]=1;
            }
        }    
        }
    }
    //console.log(dismissedBatsman);
    let finalResultOfDismissedBatsmanByBowler={};
    for(let item of deliveriesData){
        let sortDismissedBatsman = [];
        for (let x in dismissedBatsman[item.player_dismissed] ) {
            sortDismissedBatsman.push([x, dismissedBatsman[item.player_dismissed][x]]);
        }
        sortDismissedBatsman.sort(function(firstValue, secondValue) {
            return firstValue[1] - secondValue[1];
        });
        //onsole.log(sortDismissedBatsman);
        if(sortDismissedBatsman.length>0){
           finalResultOfDismissedBatsmanByBowler[item.player_dismissed]=sortDismissedBatsman[sortDismissedBatsman.length-1][1];
        }
        //console.log(sortDismissedBatsman);
        //finalResult[item.player_dismissed]=sortDismissedBatsman[sortDismissedBatsman.length-1][0];
    }
    return finalResultOfDismissedBatsmanByBowler;
}
module.exports=bowlerDismissedBatsman;