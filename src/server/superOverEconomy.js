function superOverEconomy(deliveries){
    let calculateSuperOverOfEachBowler={};
    let calculateRunsByEachBowlerInSuperOver={};
    let calculateEconomyOfEachBowler={};
    let finalResultOfSuperEconomy={};
    for(let item of deliveries){
        //onsole.log(item.is_super_over);
       if(item.is_super_over>0){
           if(calculateSuperOverOfEachBowler[item.bowler]){
                calculateSuperOverOfEachBowler[item.bowler]+=1;
           }else{
               calculateSuperOverOfEachBowler[item.bowler]=1;
           }    
       }
    }
    for(let item of deliveries){
        if(item.is_super_over>0){
            //console.log(item.is_super_over);
           if(calculateRunsByEachBowlerInSuperOver[item.bowler]){
               calculateRunsByEachBowlerInSuperOver[item.bowler]+=parseInt(item.total_runs)-(parseInt(item.bye_runs)+parseInt(item.legbye_runs)+parseInt(item.penalty_runs));
           }else{
               calculateRunsByEachBowlerInSuperOver[item.bowler]=parseInt(item.total_runs)-(parseInt(item.bye_runs)+parseInt(item.legbye_runs)+parseInt(item.penalty_runs));
           }
        }
    }
    for(let item of deliveries){
        if(typeof calculateSuperOverOfEachBowler[item.bowler]!=undefined && calculateRunsByEachBowlerInSuperOver[item.bowler]){
            //console.log(calculateSuperOverOfEachBowler[item.bowler],calculateSuperOverOfEachBowler[item.bowler]);
           calculateEconomyOfEachBowler[item.bowler]=calculateRunsByEachBowlerInSuperOver[item.bowler]/calculateSuperOverOfEachBowler[item.bowler];
        }   
    }
    let sortBowlerAccordingToEconomy = [];
    for (let x in calculateEconomyOfEachBowler) {
        sortBowlerAccordingToEconomy.push([x, calculateEconomyOfEachBowler[x]]);
    }
    sortBowlerAccordingToEconomy.sort(function(firstValue, secondValue) {
        return firstValue[1] - secondValue[1];
    });
    finalResultOfSuperEconomy[sortBowlerAccordingToEconomy[0][0]]=sortBowlerAccordingToEconomy[0][1];
    return finalResultOfSuperEconomy;
}
module.exports=superOverEconomy;

