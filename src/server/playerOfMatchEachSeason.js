function  playerOfMatchEachSeason(matches){
    let playerOfMatchSeason={};
    for(let item of matches){
        if (playerOfMatchSeason[item.season])
        {
            if(playerOfMatchSeason[item.season][item.player_of_match])
           {
               playerOfMatchSeason[item.season][item.player_of_match]+=1;
            }
            else
            {
               playerOfMatchSeason[item.season][item.player_of_match]=1;
            }
        }
        else
        {
           playerOfMatchSeason[item.season]={};
           playerOfMatchSeason[item.season][item.player_of_match]=1;
        }
     } 
    //console.log(playerOfMatchSeason); 
    let finalResultOfPlayerOfMatchSeason={};
    for(let item of matches){
        let sortPlayerOfMatch = [];
        for (let player in playerOfMatchSeason[item.season] ) {
             sortPlayerOfMatch.push([player, playerOfMatchSeason[item.season][player]]);
        }
        sortPlayerOfMatch.sort(function(firstValue, secondValue) {
            return firstValue[1] - secondValue[1];
        });
        finalResultOfPlayerOfMatchSeason[item.season]=sortPlayerOfMatch[sortPlayerOfMatch.length-1][0];

    }
    return finalResultOfPlayerOfMatchSeason;
    
}
module.exports=playerOfMatchEachSeason;