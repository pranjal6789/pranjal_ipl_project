function extraRuns(matchesData,deliveriesData){
    const arrayOfMatchId=[];
    const extraRunsByEachTeam={};
    //here we are finding the match id
    for(let item of matchesData){
        if(item.season==='2016'){
            arrayOfMatchId.push(item);
        }
    }
    for(let i=0;i<arrayOfMatchId.length;i++){
        for(let item of deliveriesData){
            if(arrayOfMatchId[i].id===item.match_id){
                if(extraRunsByEachTeam[item.bowling_team]){
                    extraRunsByEachTeam[item.bowling_team]+=parseInt(item.extra_runs);
                }else{
                    extraRunsByEachTeam[item.bowling_team]=parseInt(item.extra_runs);
                }    
            }
        }
    }
    return extraRunsByEachTeam;

}

module.exports=extraRuns;