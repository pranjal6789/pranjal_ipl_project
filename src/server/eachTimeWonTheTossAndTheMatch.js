function eachTimeWonTheTossAndTheMatch(matchesData){
    let wonTossAndMatchByTeam={};
    for(let item of matchesData){
        if(item.toss_winner===item.winner){
            if(wonTossAndMatchByTeam[item.toss_winner]){
                wonTossAndMatchByTeam[item.toss_winner]+=1;
            }else{
                wonTossAndMatchByTeam[item.toss_winner]=1;
            }    
        }
    }
    return wonTossAndMatchByTeam;
}
module.exports=eachTimeWonTheTossAndTheMatch;