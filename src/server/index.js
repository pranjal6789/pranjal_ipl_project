const csv=require("csv-parser");
const fs=require("fs");
const matchesPlayedPerYear=require('./matchesPlayedPerYear.js');
const matchesWonPerTeam = require("./matchesWonPerTeam.js");
const extraRuns=require("./extraRuns.js");
const mostEconomic=require("./mostEconomic.js");
const eachTimeWonTheTossAndTheMatch=require("./eachTimeWonTheTossAndTheMatch.js");
const superOverEconomy=require("./superOverEconomy.js");
const playerOfMatchEachSeason=require("./playerOfMatchEachSeason.js");
const strikeRateOfEachBatsMan=require("./strikeRateOfEachBatsMan.js");
const bowlerDismissedBatsman=require("./bowlerDismissedBatsman.js");
//const deliveriesDataPath=path.resolve("src/data/deliveriesData.csv");
    const matchesData = [];
    fs.createReadStream(
    "/home/ubuntu/Desktop/pranjal_ipl_project/src/data/matches.csv"
)
    .pipe(csv())
    .on("data", (data) => matchesData.push(data))
    .on("end", () => {
        const matchesPlayed=matchesPlayedPerYear(matchesData);
        const matchesWon=matchesWonPerTeam(matchesData);
        const matchesWonAndToss=eachTimeWonTheTossAndTheMatch(matchesData);
        const playerOfMatch=playerOfMatchEachSeason(matchesData);
        const FileSystem = require("fs");
        FileSystem.writeFile('src/public/output/matchesPerYear.json', JSON.stringify(matchesPlayed), (err) => {
        if (err) throw err;
       
        });
        FileSystem.writeFile('src/public/output/matchesWonPerTeam.json', JSON.stringify(matchesWon), (err) => {
            if (err) throw err;
       });
       FileSystem.writeFile('src/public/output/winTossAndMatch.json', JSON.stringify(matchesWonAndToss), (err) => {
        if (err) throw err;
     });
     FileSystem.writeFile('src/public/output/playerOfMatch.json', JSON.stringify(playerOfMatch), (err) => {
        if (err) throw err;
     });

    });  

    const deliveriesData=[];
    fs.createReadStream(
        "/home/ubuntu/Desktop/pranjal_ipl_project/src/data/deliveries.csv"
    )
    .pipe(csv())
    .on("data", (data) => deliveriesData.push(data))
    .on("end" , () => {
        const extras=extraRuns(matchesData,deliveriesData);
        //console.log(results1);
        const economic=mostEconomic(matchesData,deliveriesData);
        const superOver=superOverEconomy(deliveriesData);
        const strikerate=strikeRateOfEachBatsMan(matchesData,deliveriesData);
        const dismissedBatsman=bowlerDismissedBatsman(deliveriesData);
        const FileSystem=require("fs");
        FileSystem.writeFile('src/public/output/extraRuns.json',JSON.stringify(extras), (err)=>{
        if (err) throw err;
        });
        FileSystem.writeFile('src/public/output/mostEconomic.json',JSON.stringify(economic), (err)=>{
            if (err) throw err;
            });
        FileSystem.writeFile('src/public/output/superOverEconomy.json',JSON.stringify(superOver), (err)=>{
                if (err) throw err;
                });
        FileSystem.writeFile('src/public/output/strikeRate.json',JSON.stringify(strikerate), (err)=>{
                    if (err) throw err;
                    }); 
        FileSystem.writeFile('src/public/output/dissmissedBatsman.json',JSON.stringify(dismissedBatsman), (err)=>{
                        if (err) throw err;
                        })               
    });
