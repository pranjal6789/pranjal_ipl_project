function extraRunsPerTeam(){
    fetch("./output/extraRuns.json")
    .then(response=> response.json())
    .then((data)=>{
        //console.log(data);
        let newData=[];
        for(let index in data){
            newData.push({name:index,data:[data[index]]})
        }
        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'ExtraRuns'
            },
            xAxis: {
                //categories:data1,
                categories:['Rising Pune Supergiants','Mumbai Indians','Kolkata Knight Riders','Delhi Daredevils','Gujarat Lions','Kings XI Punjab','Sunrisers Hyderabad'],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Runs',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            /*tooltip: {
                valueSuffix: ' millions'
            },*/
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
    
            },
            series:newData,
        });
})
    .catch((error)=>{
        console.error(error);
    })   
}
extraRunsPerTeam();
function matchesPerYear(){
    fetch('./output/matchesPerYear.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        for(let index in data){
            newData.push({name:index,y:data[index]});
        }
        Highcharts.chart('PerYearMatch', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Matches Per Year'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total Matches Per Year'
                }
        
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        //format: '{point.y:.1f}%'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },
        
            series: [
                {
                    name: "Year",
                    colorByPoint: true,
                    data:newData,
                }
            ]
        });
    }
    ).catch((error)=>{
        console.log(error);
    })
}
matchesPerYear();
function winTossAndMatch(){
    fetch('./output/winTossAndMatch.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        for(let index in data){
            newData.push({name:index,y:data[index],drilldown:index});
        }
        Highcharts.chart('win toss and match', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Win Toss and Match'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },
        
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y}'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },
        
            series: [
                {
                    name: "Team",
                    colorByPoint: true,
                    data: newData,
                }
            ],
        
        });

    }).catch((error)=>{
        console.log(error);
    })
}
winTossAndMatch();
function matchesWonPerTeam(){
    fetch('./output/matchesWonPerTeam.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        let season=[];
        let team=new Set ();
        for(let item in data){
           season.push(item);
           for(let index in data[item]){
               team.add(index);

           }
        }
        let wholeTeam=Array.from(team);
        for(let item in wholeTeam){
            let winningTeamData=[];
           for(let index in season){
               if(data[season[index]].hasOwnProperty(wholeTeam[item])){
                  winningTeamData.push(data[season[index]][wholeTeam[item]]);
               }else{
                   winningTeamData.push(0);
               }
           }
           newData.push({name:wholeTeam[item],data:winningTeamData})
        }
        //console.log(newData);
        Highcharts.chart('Won Per Team', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Matches Won By Teams In Each Season'
            },
            xAxis: {
                categories:wholeTeam,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall (mm)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: newData,
        });

        
        
    }).catch((error)=>{
        console.log(error);
    })
}
matchesWonPerTeam();

function mostEconomic(){
    fetch('./output/mostEconomic.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        for(let index in data){
            newData.push([index,data[index]]);
        }
        Highcharts.chart('most economy', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Most Economy'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Top Economical Bowler: <b>{point.y:.1f} </b>'
            },
            series: [{
                name: 'Bowlers',
                data: newData,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });

    }).catch((error)=>{
        console.log(error);
    })
}
mostEconomic();
function strikeRate(){
    fetch('./output/strikeRate.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        let xAxisData=[];
        for(let index in data){
            previous=data[index];
           for(let item in previous){
               newData.push(previous[item]);
               xAxisData.push(index);
           }
        }
        //console.log(xAxisData);
        Highcharts.chart('strike-rate', {
            title: {
                text: 'Virat Kohli Strike Rate'
            },
            xAxis: {
                categories:xAxisData,
            },
            series: [{
                type: 'column',
                colorByPoint: true,
                data: newData,
                showInLegend: false
            }]
        });
           

        }); 
    
}
strikeRate();
function superOver(){
    fetch("./output/superOverEconomy.json")
    .then(resolve=>resolve.json())
    .then((data)=>{
        let newData=[];
        for(let item in data){
            newData.push({name:item,y:data[item]});
        }
        Highcharts.chart('super over', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Best Super Over Economy'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Economy of best super over'
                }
        
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },
        
            series: [
                {
                    name: "Browsers",
                    colorByPoint: true,
                    data: newData,
                }
            ],
        });


    })
}
superOver();
function playerOfMatch(){
    fetch('./output/playerOfMatch.json')
    .then(resolve=>resolve.json())
    .then((data)=>{
        let newData=[]; 
        for(let index in data){
           newData.push(['season',index]);
        }
        for(let index in data){
            newData.push([index,data[index]]);
        }
        Highcharts.chart('player of match', {
            chart: {
                height: 600,
                inverted: true
            },
        
            title: {
                text: 'Player of Match for Each Season'
            },
        
            accessibility: {
                point: {
                    descriptionFormatter: function (point) {
                        var nodeName = point.toNode.name,
                            nodeId = point.toNode.id,
                            nodeDesc = nodeName === nodeId ? nodeName : nodeName + ', ' + nodeId,
                            parentDesc = point.fromNode.id;
                        return point.index + '. ' + nodeDesc + ', reports to ' + parentDesc + '.';
                    }
                }
            },
        
            series: [{
                type: 'organization',
                name: 'Highsoft',
                keys: ['from', 'to'],
                data: newData,
                levels: [{
                    level: 0,
                    color: 'silver',
                    dataLabels: {
                        color: 'black'
                    },
                    height: 25
                }, {
                    level: 1,
                    color: 'silver',
                    dataLabels: {
                        color: 'black'
                    },
                    height: 25
                }, {
                    level: 2,
                    color: '#980104'
                }],
                colorByPoint: false,
                color: '#007ad0',
                dataLabels: {
                    color: 'white'
                },
                borderColor: 'white',
                nodeWidth: 65
            }],
            tooltip: {
                outside: true
            },
            exporting: {
                allowHTML: true,
                sourceWidth: 800,
                sourceHeight: 600
            }
        
        });
        
       

    })
}
playerOfMatch();
function playerDismissed(){
    fetch('./output/dissmissedBatsman.json')
    .then(response=>response.json())
    .then((data)=>{
        let newData=[];
        for(let index in data){
            newData.push({name:index,y:data[index]});
        }
        //console.log(newData);
        Highcharts.chart('player dismissed by other', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Player Dismissed By other Player'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Batsman'
                }
        
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },
        
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },
        
            series: [
                {
                    name: "Player Dismissed by other player",
                    colorByPoint: true,
                    data: newData,
                }
            ],
            
        });
    })
}
playerDismissed();